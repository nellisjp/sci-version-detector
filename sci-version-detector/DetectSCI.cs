﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;

namespace sci_version_detector
{
    public class Game
    {
        public String name { get; set; }
        public String sciVersion { get; set; }
        public String interpVersion { get; set; }
        public String gameVersion { get; set; }
        public String date { get; set; }
        public String notes { get; set; }
    }

    public class Configs
    {
        public String option { get; set; }
        public String notes { get; set; }
    }

    class DetectSCI
    {
       
        static public String UnpackEXE(string exeName)
        {
              byte[] _bytes = null;
              Path.GetFileName(exeName);

                if (!File.Exists("unlzexe.exe"))
                {
                    MessageBox.Show("Unable to decompress " + Path.GetFileName(exeName) + ". The decompressor \"unlzexe.exe\" \nnot found.  It should be in the same folder as this application");
                    return(null);
                }

                string exeTempPath = Path.GetTempPath() + Path.GetFileName(exeName);
                
 
                try
                {
                    File.Copy(exeName, exeTempPath, true);
                }
                catch
                {
                }
 
                Process unlzexe = new Process();
                string decompPath = Path.GetTempPath() + Path.GetFileName(exeName) +"NEW";
                try
                {
                    unlzexe.StartInfo.FileName = @"unlzexe.exe";
                    unlzexe.StartInfo.Arguments = "\"" + exeTempPath + "\"";
                    unlzexe.StartInfo.UseShellExecute = false;
                    unlzexe.StartInfo.RedirectStandardError = true;
                    unlzexe.StartInfo.CreateNoWindow = true;
                    unlzexe.Start();
                    unlzexe.WaitForExit();
 
                    if (File.Exists(decompPath))
                    {
                        try
                        {
                            _bytes = File.ReadAllBytes(decompPath);
                        }
                        catch
                        {
                        }

                        try
                        {
                            File.Copy(decompPath, exeTempPath, true);
                            try
                            {
                                File.SetAttributes(decompPath, FileAttributes.Normal);
                                File.Delete(decompPath); 
                            }
                            catch { }
                        }
                        catch
                        {
                        }
                    }

                    try
                    {
                        File.SetAttributes(exeTempPath, FileAttributes.Normal);
                        File.Delete(exeTempPath);
                    }
                    catch
                    {
                    }
                    try
                    {
                        return (System.Text.Encoding.Default.GetString(_bytes));
                    }
                    catch
                    {
                        return (null);
                    }
                }
                catch (Win32Exception ex)
                {
                    string Err = ("Error: " + ex.Message);
                    MessageBox.Show(Err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                return (null);
        }

        static public String GetInterpreterVersion(string inputStr)
        {
            // get version number
            Regex r1 = new Regex(@"([a-z|A-Z|0-9]\.[a-z|A-Z|0-9]{3}\.[a-z|A-Z|0-9]{3})");
            Match match = r1.Match(inputStr);

            if (match.Success)
            {
                // check for x.yyy.zzz interps
                if (match.Groups[1].Value == "x.yyy.zzz")
                {
                   // (HOYLE)
                   r1 = new Regex(@"\x01\x2E\x00\xB2\x00\x0D\x00\xB0x\.yyy\.zzz\x09\x00\xB2\x00\x47\x00");
                   match = r1.Match(inputStr);
                   if(match.Success)
                   {
                       return("x.yyy.zzz (HOYLE)");
                   }

                   // (JONES 1.000.060)  
                   //r1 = new Regex(@"\xC1\x18\x9B\x18\x0C\x19\xE6\x18\x57\x19\x31\x19x\.yyy\.zzz");
                   r1 = new Regex(@"\x18\x0C\x19\xE6\x18\x57\x19\x31\x19x\.yyy\.zzz");
                   match = r1.Match(inputStr);
                   if (match.Success)
                   {
                       return ("x.yyy.zzz (JONES 1.000.060)");
                   }

                   /* JONES STUB, cannot identify which executables match:
                   Jones in the Fast Lane VGA 1.0 (CD version) (SCI1, no debugger)
                   Jones in the Fast Lane VGA 1.0 (CD version) (SCI1, with debugger)
                   Jones in the Fast Lane VGA 1.0 (Windows CD version) (SCI1)
                   */

                   // (KQ5)
                   r1 = new Regex(@"\xC0\x00\xAA\x00{5}\x01\x00{77}x\.yyy\.zzz\x00{71}");
                   match = r1.Match(inputStr);
                   if (match.Success)
                   {
                       return ("x.yyy.zzz (KQ5)");
                   }

                   /* MIXED-UP STUB, cannot identify which executable matches (we have 2 variants):
                   Mixed-Up Mother Goose (CD version) (SCI1, with debugger)
                   */

                   // LB2DEMO & ECO QUEST DEMO (these should be separate versions?)
                   r1 = new Regex(@"\xA0\x00\x0A\x00\xC0\x00\xAA\x00{5}\x01\x00{15}x\.yyy\.zzz");
                   match = r1.Match(inputStr);
                   if (match.Success)
                   {
                       return ("x.yyy.zzz (UNKNOWN)");   // unknown for now
                   }

                   // 'unknown' x.yyy.zzz version
                   return ("x.yyy.zzz (UNKNOWN)");
                }
                return (match.Groups[1].Value);
            }
            else
            {
                r1 = new Regex(@"(No version number)");
                match = r1.Match(inputStr);
                if (match.Success)
                {
                    return (match.Groups[1].Value);
                }
                return (null);
            }
        }

        static public String GuessSCIMajorVersion(String inputStr)
        {
            Regex r1 = new Regex(@"vpstnmwfc");
            Match match = r1.Match(inputStr);

            if (match.Success)
            {
                return ("SCI0/SCI01?");
            }

            return (null);
        }

        static public String GetSCIMajorVersion(String versionNum, List<Game> gameList)
        {
            // can't lookup the major SCI version if this is the interp.  
            // we will need to do it a different way
            if (versionNum == "x.yyy.zzz")
            {
                return (null);
            }

            foreach (Game game in gameList)
            {
                if (game.interpVersion == versionNum)
                {
                    return (game.sciVersion);
                }
            }
            return (null);
        }

        // Import XML data created from: http://web.archive.org/web/20071130073710/sierra.8bit.co.uk/SVLIST091.TXT
        static public List<Game> LoadGameXMLFile(String inputXML)
        {
            List<Game> gameList;
            XDocument xdoc = null;

            try
            {
                xdoc = XDocument.Load(inputXML);
            }
            catch
            {
                return (null);
            }

            gameList = new List<Game>();

            gameList = (from xml in xdoc.Elements("Document").Elements("Row")
                        select new Game
                        {
                            name = xml.Element("Descrip").Value,
                            sciVersion = xml.Element("sciVersion").Value,
                            interpVersion = xml.Element("interpreterVersion").Value,
                            gameVersion = xml.Element("gameVersion").Value,
                            date = xml.Element("date").Value,
                            notes = xml.Element("notes").Value
                        }).ToList();

            gameList.Sort((a, b) => String.Compare(a.name, b.name));
            return (gameList);
        }

        static public List<Game> GetKnownGames(String versionNum, List<Game> gameList)
        {
            List<Game> matchedGameList = new List<Game>();

            // can't lookup the games if this is the interp.  
            // we will need to do it a different way
            if (versionNum == "x.yyy.zzz")
            {
                return (matchedGameList);
            }

            // look up the SCI major version & games
            foreach (Game game in gameList)
            {
                if (game.interpVersion == versionNum)
                {
                    matchedGameList.Add(game);
                }
            }
            return (matchedGameList);
        }

        static public List<Configs> GetConfigTokens(String inputStr)
        {

            /*  Config dirtyTokens live between these boundaries:
            SCI0 (early):  20 09 3D 3B 2C 00 + misc strings + "Please run the Install program. " & "Out of configuration storage."
            SCI0 (late):   20 09 3D 3B 2C 00 + misc strings + "Please run the Install program. " & "Out of configuration storage."
            SCI01:         20 09 3D 3B 2C 00 + misc strings + "Please run the Install program. " & "Out of configuration storage."
            SCI1 (EGA):    20 09 3D 3B 2C 00 + misc strings + "Please run the Install program. " & "Out of configuration storage."
            SCI1 (early):  20 09 3D 3B 2C 00 + misc strings + "Please run the Install program. " & "Out of configuration storage."
            SCI1 (middle)  20 09 3D 3B 2C 00 & "Out of configuration storage."
            SCI1 (late):   20 09 3D 3B 2C 00 & "Out of configuration storage."
            SCI1 (unknown) 20 09 3D 3B 2C 00 & "Out of configuration storage."
            SCI1_1         20 09 3D 3B 2C 00 & "Out of configuration storage."

            SCI2   "view 00 vocab 00  &  "resource.cfg 00 Config file name can not exceed...."
            SCI21  "view 00 vocab 00  &  "00 Config file name can not exceed...."
            SCI3   "view 00 vocab 00  &  "00 Config file name can not exceed...."
             
            Others (incomplete)
            Island of Dr Brain Demo 20 09 3D 3B 2C 00 & %c%4uK (25 63 25 34 75 4B) - with 13 leading characters
            LSL6:         "sierra.ini not found" & "Sierra\x00Cannot run"
            LSL7, SCI3:   "(Game specific) & "Can't find config"  
            */

            List<Configs> configs = new List<Configs>();
            // get the possible config dirtyTokens
            Regex r1 = null;

            Match match = null;
            String configArea = null;
            bool flipPairs = false;

            if (configArea == null)
            {
                r1 = new Regex(@"(?<=\x20\x09\x3D\x3B\x2C\x00)(.*)(?=Out of configuration storage)", RegexOptions.Singleline);
                match = r1.Match(inputStr);
                // SCI 0 through SCI 1.1

                // remove the RESOURCE.CFG & missing file from the beginning of the match

                if (match.Success)
                {
                    String data = match.Groups[1].Value;

                    if (data.Contains("Install program"))
                    {
                        configArea = data.Substring(data.IndexOf("Install program") + 15);
                    }
                    else
                    {
                        configArea = data;
                    }
                }
            }
           
            if (configArea == null)
            {
                r1 = new Regex(@"(?<=view\x00vocab\x00)(.*)(?=Config file name can not exceed)", RegexOptions.Singleline);
                match = r1.Match(inputStr);
                if (match.Success)
                {
                    // SCI 2 & 3
                    configArea = match.Groups[1].Value;
                }
            }

            if (configArea == null)
            {
                r1 = new Regex(@"(?<=\x20\x09\x3D\x3B\x2C\x00)(.*)(?=.{13}\x25\x63\x25\x34\x75\x4B)", RegexOptions.Singleline);
                match = r1.Match(inputStr);
                if (match.Success)
                {
                    configArea = match.Groups[1].Value;
                }
            }

            if (configArea == null)
            {
                r1 = new Regex(@"(?<=sierra\.ini not found)(.*)(?=Display\x00Small Window)", RegexOptions.Singleline);
                match = r1.Match(inputStr);
                if (match.Success)
                {
                    configArea = match.Groups[1].Value;
                }
            }

            if (configArea == null)
            {
                r1 = new Regex(@"(?<=\(Game specific\))(.*)(?=Can't find config)", RegexOptions.Singleline);
                match = r1.Match(inputStr);
                if (match.Success)
                {
                    configArea = match.Groups[1].Value;
                    flipPairs = true;   // key/value pairs are in reversed order
                }
            }


            if (configArea != null)
            {
                string[] dirtyTokens = Regex.Split(configArea, @"\x00");
                List<String> tokens = new List<String>();

                // data cleanup
                for (int i = 0; i < dirtyTokens.Length; i++)
                {

                    dirtyTokens[i] = Regex.Replace(dirtyTokens[i], @"^[\x00]+", "");
                    dirtyTokens[i] = Regex.Replace(dirtyTokens[i], @"[\x00]+$", "");

                    // clean up the dirtyTokens (not the notes)
                    if (!(dirtyTokens[i].Contains(" ") || dirtyTokens[i].Contains("(") || dirtyTokens[i].Contains(")")))
                    {
                        dirtyTokens[i] = Regex.Replace(dirtyTokens[i], @"^.[^a-zA-Z0-9]", "");
                    }

                    // skip bad data
                    if (i + 1 < dirtyTokens.Length)
                    {
                        if ( (dirtyTokens[i].Contains(" ") || dirtyTokens[i].Contains("(") || dirtyTokens[i].Contains(")"))
                          && (dirtyTokens[i + 1].Contains(" ") || dirtyTokens[i + 1].Contains("(") || dirtyTokens[i + 1].Contains(")"))
                           )
                        {
                            for (int j = i; j < dirtyTokens.Length; j++)
                            {
                                if (!(dirtyTokens[j].Contains(" ") || dirtyTokens[j].Contains("(") || dirtyTokens[j].Contains(")")))
                                {
                                    i = j;
                                    break;
                                }
                            }
                        }
                    }

                    if (     dirtyTokens[i] != "resource.cfg" 
                          && !dirtyTokens[i].Contains("Error:") 
                          && !dirtyTokens[i].Contains("sierra.ini")
                          && !dirtyTokens[i].Equals("  ")
                          && !dirtyTokens[i].Contains("   ")
                          && dirtyTokens[i].Length > 1)
                    {
                        tokens.Add(dirtyTokens[i]);
                    }
                }

                for (int i = 0; i < tokens.Count; i++)
                {
                    if (tokens[i].Length != 0)
                    {
                        if (tokens[i].Contains(" ") || tokens[i].Contains("(") || tokens[i].Contains(")"))
                        {
                            if (flipPairs)
                            {
                                configs.Add(new Configs() { option = tokens[i-1], notes = tokens[i] });
                            }
                            else
                            {
                                configs.Add(new Configs() { option = tokens[i + 1], notes = tokens[i] });
                            }
                            i++;
                        }
                        else
                        {
                            if (!flipPairs)
                            {
                                configs.Add(new Configs() { option = tokens[i], notes = "" });
                            }
                            else
                            {
                                if(i >= 1)
                                {
                                    configs.Add(new Configs() { option = tokens[i-1], notes = "" });
                                }
                            }
                        }
                    }
                }
                configs.Sort((a, b) => String.Compare(a.option, b.option));
                return (configs);
            }
            return (null);
        }

    }
}
