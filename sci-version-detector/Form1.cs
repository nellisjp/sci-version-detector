﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml.Linq;
using System.Diagnostics;

namespace sci_version_detector
{
    public partial class Form1 : Form
    {

        public class GameGrid
        {
            public String name { get; set; }
            public String sciVersion { get; set; }
            public String gameVersion { get; set; }
            public String date { get; set; }
            public String notes { get; set; }
        }


        public List<Game> gameList;
        public List<Game> matchedGameList;

        public Form1()
        {
            InitializeComponent();
        }


        private void Detect(string inputEXE)
        {
            byte[] _bytes = null;
            List<Configs> configs = new List<Configs>();
       
            executableLabel.Text = String.Format("Executable: {0}", Path.GetFileName(inputEXE));

            // consume the executable
            try
            {
                _bytes = File.ReadAllBytes(inputEXE);
            }
            catch
            {
                MessageBox.Show("Could not open {0}", inputEXE);
            }

            // validation
            if (_bytes[0] != 'M' || _bytes[1] != 'Z')
            {
                MessageBox.Show("{0} is not a valid executable", inputEXE);
            }

            // attempt to decompress
            String str = null;
            String decompStr = DetectSCI.UnpackEXE(inputEXE);
            if (decompStr != null)
            {
                str = decompStr;
            }
            else
            {
                str = System.Text.Encoding.Default.GetString(_bytes);
            }

            String versionNum = DetectSCI.GetInterpreterVersion(str);

            /*
            String str = System.Text.Encoding.Default.GetString(_bytes);

            String versionNum = DetectSCI.GetInterpreterVersion(str);

            // if detection fails, try to decompress the executable
            if (versionNum == null)
            {
                String decompStr = DetectSCI.UnpackEXE(inputEXE);
                versionNum = DetectSCI.GetInterpreterVersion(decompStr);
                if (versionNum != null)
                {
                    str = decompStr;
                }
            }
            */

            if (versionNum != null)
            {
                numberLabel.Text = String.Format("Interpreter Version: {0}", versionNum);
            }
            else
            {
                numberLabel.Text = String.Format("Interpreter Version: Detection failed");
            }

            String majorVersionNum = DetectSCI.GetSCIMajorVersion(versionNum, gameList);

            if(majorVersionNum == null)
            {
               majorVersionNum = DetectSCI.GuessSCIMajorVersion(str);
               if (majorVersionNum == null)
               {
                   versionLabel.Text = String.Format("SCI Version: UNKNOWN");
               }
               else
               {
                   versionLabel.Text = String.Format("SCI Version: {0}", majorVersionNum);
               }
            } 
            else 
            {
                versionLabel.Text = String.Format("SCI Version: {0}", majorVersionNum);
            }

            matchedGameList = new List<Game>();
            matchedGameList = DetectSCI.GetKnownGames(versionNum, gameList);

            List<GameGrid> columnsSubSet;
            columnsSubSet = (from G in matchedGameList
                             select new GameGrid
                             {
                                 name = G.name,
                                 sciVersion = G.sciVersion,
                                 gameVersion = G.gameVersion,
                                 date = G.date,
                                 notes = G.notes
                             }).ToList();
            dataGridView1.DataSource = columnsSubSet;

            // set the widths & column headings
            dataGridView1.Columns[0].HeaderText = "Game Name";
            dataGridView1.Columns[0].MinimumWidth = 200;
            dataGridView1.Columns[1].FillWeight = 200;

            dataGridView1.Columns[1].HeaderText = "SCI Ver.";
            dataGridView1.Columns[1].MinimumWidth = 45;
            dataGridView1.Columns[1].FillWeight = 45;

            dataGridView1.Columns[2].HeaderText = "Game Ver.";
            dataGridView1.Columns[2].MinimumWidth = 65;
            dataGridView1.Columns[2].FillWeight = 65;

            dataGridView1.Columns[3].HeaderText = "Date";
            dataGridView1.Columns[3].MinimumWidth = 55;
            dataGridView1.Columns[3].FillWeight = 55;

            dataGridView1.Columns[4].HeaderText = "Notes";
            dataGridView1.Columns[4].FillWeight = 200;


            configs = DetectSCI.GetConfigTokens(str);

            if (configs != null)
            {
                dataGridView2.DataSource = configs;

                dataGridView2.Columns[0].HeaderText = "Option";
                dataGridView2.Columns[0].FillWeight = 35;

                dataGridView2.Columns[1].HeaderText = "Notes";
                dataGridView2.Columns[1].FillWeight = 100;

            }
            else
            {
                dataGridView2.DataSource = null;
                dataGridView2.Rows.Clear();
            }


        }

        private void browseButton_Click(object sender, EventArgs e)
        {

            OpenFileDialog file = new OpenFileDialog();
            file.Title = "Open SCI EXE File";
            file.Filter = "EXE Files|*.EXE|All Files|*.*";

            if (file.ShowDialog() == DialogResult.OK)
            {
                // reset all labels
                executableLabel.Text = "Executable:";
                versionLabel.Text = "SCI Version:";
                numberLabel.Text = "Interpreter Version:";
                Detect(file.FileName);
            }
        }


        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


            gameList = DetectSCI.LoadGameXMLFile("gameList.xml");

            if (gameList == null)
            {
                MessageBox.Show("Game list (gameList.xml) is missing.  It should be in same directory as this application, aborting.");
                Application.Exit();
            }
           

        }

        private void numberLabel_Click(object sender, EventArgs e)
        {

        }

        private void versionLabel_Click(object sender, EventArgs e)
        {

        }

        private void executableLabel_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}
